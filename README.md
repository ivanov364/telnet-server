# telnet-server

Server for telnet clients


<h2>To build</h2>

```
gcc server.c -o server
```



<h3>Example</h3>

for server
```
./server 9034
```

*<b>9034 is PORT</b>

for telnet
```
telnet ::1 9034
```

Thanks to <a href="https://beej.us/guide/bgnet/html/">Beej's Guide to Network Programming</a>